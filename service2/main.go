package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/hello", func(writer http.ResponseWriter, request *http.Request) {
		_, _ = fmt.Fprintf(writer, "%v", "\"Hello World\" (C) service2")
	})

	_ = http.ListenAndServe(":8080", nil)
}
