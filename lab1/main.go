package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	name := os.Getenv("NAME")
	team := os.Getenv("TEAM")
	port := os.Getenv("PORT")
	svc2Host := os.Getenv("SVC2_HOST")
	svc2Port := os.Getenv("SVC2_PORT")
	if name == "" {
		fmt.Println("Вы должны задать переменную NAME")
	}
	if team == "" {
		fmt.Println("Вы должны задать переменную TEAM")
	}
	if port == "" {
		fmt.Println("Вы должны задать переменную PORT")
	}

	http.HandleFunc("/hello", func(writer http.ResponseWriter, request *http.Request) {
		_, _ = fmt.Fprintf(writer, "Hello, %v from %v\n", name, team)

		if svc2Host != "" && svc2Port != "" {
			url := fmt.Sprintf("http://%v:%v/hello", svc2Host, svc2Port)
			resp, err := http.Get(url)
			if err != nil {
				_, _ = fmt.Fprintf(writer, "Error communicating with service2: %v", err.Error())
			}

			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				_, _ = fmt.Fprintf(writer, "Error communicating with service2: %v", err.Error())
			}

			_, _ = fmt.Fprintf(writer, "Responce from service2: %v", string(body))
		}

	})

	portLine := fmt.Sprintf(":%v", port)
	fmt.Printf("Сервер прослушивается по адресу %s/hello \n", portLine)
	_ = http.ListenAndServe(portLine, nil)
}
